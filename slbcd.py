#!/usr/bin/env python3 -u

# slbcd.py - static/simple load-balanced client daemon

#   slbcd is a simple load-balanced client daemon that will produce
#   round-robin load balancing. slbcd returns a version 2 LBCD reply
#   with reasonable (mostly hard-coded) values.

import time, socket, struct

tzero = int(time.time())
                         # Anatomy of a LBCD version 2 packet
version  = 2             # protocol version
uid      = 0             # requester's unique request id
op       = 1             # operation requested (request/reply=1)
status   = 1             # status (okay=1)
btime    = tzero - 3600  # boot time
ctime    = tzero         # current time
utime    = tzero         # time user information last changed
l1       = 100           # load * 100 last minute
l5       = 100           # load * 100 last five minutes
l15      = 100           # load * 100 last fifteen minutes
tot      = 0             # total number of users logged in
uniq     = 0             # total number of unique users
console  = 0             # true if someone on console
reserved = 0             # future use, padding ...

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(('', 4330))

while True:
    data, address = sock.recvfrom(1024)
    if len(data) < 8: continue
    uid, op = struct.unpack_from("!hhhh",data)[1:3]  # grab id and operation
    if op != 1: continue                             # skip it if it's not a request
    reply = struct.pack("!HHHHLLLHHHHHBBxxxx",       # build the reply, with version 3
                        version, uid, op, status,    #   template to appease lbcdclient
                        btime, int(time.time()),
                        utime, l1, l5, l15, tot,
                        uniq, console, reserved)
    sock.sendto(reply, address)
