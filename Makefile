# Makefile for installing PowerDNS version of lbnamed

# Files installed:

#   lbbe           -- load-balancing back end
#   poller         -- polls load-balanced name hosts
#   poller.config  -- site specific load-balancing names config
#   LBCD.pm        -- LBCD protocol module

# Note: PowerDNS must also be configured. See ``PowerDNS
#       Configuration'' in README.md

NAME=power-lbbe

FILES=poller.config LBCD.pm lbbe poller

ALL_FILES=Makefile README.md $(FILES) slbcd

VERSION=1.0.0
RELEASE=$(NAME)-$(VERSION)

check: $(ALL_FILES)
	@echo "  The files are ready. Now what? Targets: install/update, dist/release, clean"

install update: /etc/poller.config /usr/sbin/lbbe /usr/sbin/poller /usr/share/perl5/LBCD.pm /var/lib/poller

/etc/poller.config: poller.config
	install -D -m 644 $< $@

/usr/sbin/lbbe: lbbe
	install -D -m 755 $< $@

/usr/sbin/poller: poller
	install -D -m 755 $< $@

/usr/share/perl5/LBCD.pm: LBCD.pm
	install -D -m 644 $< $@

/var/lib/poller:
	 mkdir -p $@

dist release: $(RELEASE).tar.gz

$(RELEASE).tar.gz: $(RELEASE).tar
	gzip -f $^

$(RELEASE).tar: $(ALL_FILES)
	ln -s . $(RELEASE)
	tar cf $@ $(addprefix $(RELEASE)/,$^)
	rm -f $(RELEASE)

clean:
	rm -f $(NAME)-*.tar.gz poller.config.*

